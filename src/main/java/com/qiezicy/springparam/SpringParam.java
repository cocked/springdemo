package com.qiezicy.springparam;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by IntelliJ IDEA
 *
 * @author Eric.Shen
 * @date 2019/1/4
 * @time 14:06
 */

@RestController
public class SpringParam {

    @RequestMapping("/hello")
    public String helloWorld() {
        return "Hello World !";
    }

}
