package com.qiezicy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Created by IntelliJ IDEA
 *
 * @author Eric.Shen
 * @date 2019/1/4
 * @time 14:07
 */
@SpringBootApplication
public class ParamSpringappcation {
    public static void main(String[] args) {
        SpringApplication.run(ParamSpringappcation.class);
    }
}
